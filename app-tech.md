
# Seksjon for Applikasjoner

## Team Administrative systemer [🎯](https://rts.uib.no/projects/team5/agile/board)

People (4):

* @bubjb [Johanne Revheim](https://www.uib.no/user/uib/bubjb)
* @lei002 [Lill Eilertsen](https://www.uib.no/user/uib/lei002)
* @edprf [Roger Foss](https://www.uib.no/user/uib/edprf)
* @ttr031 [Trygve Trohaug](https://www.uib.no/user/uib/ttr031)

Services (35):

* Apex workspace [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=200) APEX
* Arcgis server [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=95) ArcGIS kartsystem
* Automatiserte prosesser (RPA)
* BSRS — akseptanseskjema (med betaling)
* Fakturadatabase (Basware) [📕](https://tk.app.uib.no/node/91) Innkjøpssystem (Basware PM)
* [Fakturadatabase for IT-avdelingen](https://skjema.app.uib.no/faktura) [📕](https://tk.app.uib.no/node/127)
* [Fond og legater — søknadssystem](https://fond.app.uib.no/) [📕](https://tk.app.uib.no/node/137) Fond og legater - søknadssystem
* Fragmentdatabasen
* Fremdriftsrapportering PhD (Rapportdatabase) [📕](https://tk.app.uib.no/node/125) Fremdriftsrapportering i forskerutdanningen [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=301) Fremdriftsrapportering i forskerutdanningen
* [Gjesteboliger](https://skjema.app.uib.no/gjestebolig)
* Grieg akademiet — søknad/audition
* [HMS-avvik](https://avvik.app.uib.no) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=304) Avvik
* [HMS-rapportering](https://skjema.app.uib.no/hmsrapport)
* [Kinobilletter — bestilling og betaling](https://reg.app.uib.no/kinobilletter)
* Kontakdatabasen [📕](https://tk.app.uib.no/node/134) Kontakter (kontaktdatabasen) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=226) Kontakter
* Kurspåmeldinger (Signup)
* Litteraturkiosken [📕](https://tk.app.uib.no/node/240) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=313)
* [Lydia — databasen](https://lydia.uib.no) [📕](https://tk.app.uib.no/node/126) Behovsmelding bygningsdrift (Lydia) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=60) Lydia
* [Oversikt over rom og utstyr (Brutus)](https://brutus.app.uib.no) [📕](https://tk.app.uib.no/node/143) Romutstyr (Brutus)
* [Parkering — søknad om plass og betaling](https://reg.app.uib.no/parkering) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=298) Parkering
* [Prosjektportalen](http://prosjekt.uib.no) [📕](https://tk.app.uib.no/node/320)
* Påmelding til doktorpromosjon
* Retorikkdatabasen
* Sharepoint [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=352) Sharepoint online
* Sikkert digitalt skjema (Digiforms)
* Søknad om låne-PC under eksamen [📕](https://tk.app.uib.no/node/344) Lånemaskin til digital eksamen – søknadsskjema [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=338) Lånemaskin til digital eksamen - søknadsskjema
* Søknad om sommerskole (norskkurs) Litteraturvitenskap
* Test Basware
* Tilpasset skjema med betaling (Apex) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=295) Netthandel
* Tilpasset skjema med innlogging (Apex) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=299) SIGNUP
* TVEPS-admin [📕](https://tk.app.uib.no/node/489)
* [Velferdshytter — søknadssystem](https://skjema.app.uib.no/hytter) [📕](https://tk.app.uib.no/node/32) Bestillingssystem for velferdshytter [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=199) Velferdshytter
* Verneombudslister
* Økonomiavstemming
* Økonomirapporter

| Techology | Services | People |
|-----------|---------:|-------:|
| oracle | 26 | 2 |
| apex | 24 | 1 |
| sharepoint | 2 | 1 |
| blueprism | 1 | 2 |
| digiforms | 1 | 0 |
| windows | 1 | 0 |
| sql | 0 | 2 |
| js | 0 | 1 |
| mssql | 0 | 1 |
| perl5 | 0 | 1 |
| plsql | 0 | 1 |
| ps1 | 0 | 1 |

## Team Lærings- og forskningsstøtte [🎯](https://rts.uib.no/projects/team2/agile/board)

People (7):

* @aar028 [Ana Pino](https://www.uib.no/user/uib/aar028)
* @amy098 [Anders Myren](https://www.uib.no/user/uib/amy098)
* @apa033 [Anders Paulsen](https://www.uib.no/user/uib/apa033)
* @edpck [Cato Kolås](https://www.uib.no/user/uib/edpck)
* @jbi022 [Jørgen Birkhaug](https://www.uib.no/user/uib/jbi022)
* @obr021 [Olav Bringedal](https://www.uib.no/user/uib/obr021)
* @tle029 [Torstein Leversund](https://www.uib.no/user/uib/tle029)

Services (20):

* 3.-partsportalen
* Besvarelsesrepo (eksamensarkiv)
* Digital skoleeksamen
* Epaqual [📕](https://tk.app.uib.no/node/283) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=330)
* FS-REST [📕](https://tk.app.uib.no/node/150) FSRest
* Geo-database
* [Hubro — chatbot](https://www.uib.no/hf)
* MasterArk
* [MiSide (avviklet, gjenstår som arkiv)](https://miside.uib.no) [📕](https://tk.app.uib.no/node/15) Mi side (arkiv) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=193)?
* [MittUiB](https://mitt.uib.no/) [📕](https://tk.app.uib.no/node/243) Mitt UiB [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=314)
* [Oppgaveseminar](https://mitt.uib.no/courses/11167)
* [pdf.uib.no](http://pdf.uib.no) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=322)
* [RETTE](https://rette.app.uib.no) [📕](https://tk.app.uib.no/node/487) System for Risiko og ETTErlevelse (RETTE) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=355) [🗄](https://git.app.uib.no/it/site/rette.app.uib.no)
* [Semesterbetaling](https://semesteravgift.uib.no) [📕](https://tk.app.uib.no/node/488) Semesteravgift [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=359) semesteravgift.uib.no [🗄](https://git.app.uib.no/it/site/semesteravgift.uib.no)
* [Sikker skjema for innsamling til forsking (SAFE)](https://skjema.uib.no)
* Sikker video (SAFE) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=350) Sikker Video
* [Studiekvalitetsdatabasen](https://kvalitetsbasen.app.uib.no) [📕](https://tk.app.uib.no/node/104) Studiekvalitetsbasen [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=266) Kvalitetsbasen
* [Videonotat](https://www.uib.no/læringslab/78854/opptak-av-forelesning-videonotat) [📕](https://tk.app.uib.no/node/31) Opptak av forelesninger (Videonotat) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=267)
* [webmail.uib.no](https://webmail.uib.no) [📕](https://tk.app.uib.no/node/45) E-post via nettleser (Webmail)
* [Åpne data](http://data.uib.no) [📕](https://tk.app.uib.no/node/136) Åpnedata (data.uib.no) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=258)

| Techology | Services | People |
|-----------|---------:|-------:|
| php | 7 | 6 |
| laravel | 4 | 1 |
| js | 3 | 0 |
| mysql | 1 | 2 |
| pg | 1 | 1 |
| rails | 1 | 1 |
| ruby | 1 | 1 |
| bootstrap | 1 | 0 |
| elastic | 1 | 0 |
| oracle | 1 | 0 |
| sqlite | 1 | 0 |
| vue | 1 | 0 |
| watson | 1 | 0 |
| k8s | 0 | 2 |
| rhel7 | 0 | 1 |

## Team Masterdata, Identitet og Tilgang [🎯](https://rts.uib.no/projects/team1/agile/board)

People (3):

* @atu041 [Angela Tuck](https://www.uib.no/user/uib/atu041)
* @edpeb [Elin M. Bjørndal](https://www.uib.no/user/uib/edpeb)
* @ibe085 [Ivo Beijersbergen](https://www.uib.no/user/uib/ibe085)

Services (8):

* App for kortsenteret (Siren)
* [Publisering av manntall — ansatte/studenter](https://manntall.app.uib.no) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=265) Manntall
* SEBRA (BAS/IGA) [📕](https://tk.app.uib.no/node/95) Administrasjon av brukerkonto (SEBRA) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=29) SEBRA
* Sebra REST og øvrige integrasjoner
* Telefoni
* Tildeling og administrasjon av brukerkonto [📕](https://tk.app.uib.no/node/47) Ansattkonto
* Tilgang til administrative systemer (TAS) [📕](https://tk.app.uib.no/node/246) Tilgang Administrative Systemer (TAS) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=319) Tilgang Administrative Systemer ( TAS )
* UiBhjelp [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=357) UiBhjelp (TOPdesk)

| Techology | Services | People |
|-----------|---------:|-------:|
| oracle | 6 | 3 |
| apex | 3 | 3 |
| python | 3 | 1 |
| rocket | 1 | 0 |
| topdesk | 1 | 0 |
| blueprism | 0 | 1 |

## Team Publisering [🎯](https://rts.uib.no/projects/team4/agile/board)

People (3):

* @mihho [Helge Opedal](https://www.uib.no/user/uib/mihho)
* @jhe052 [Jonas Fagnastøl Henriksen](https://www.uib.no/user/uib/jhe052)
* @mme045 [Mike Menk](https://www.uib.no/user/uib/mme045)

Services (21):

* [Bestilling av vitnemål](https://vitnemal.app.uib.no) [📕](https://tk.app.uib.no/node/158)
* [bjerknes.uib.no](https://bjerknes.uib.no)
* [Drikkeglass på Bryggen i Bergen](http://glass.app.uib.no) [📕](https://tk.app.uib.no/node/153) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=244) Drikkeglass på Bryggen i Bergen 13001700 
* DSPacc (Bora, Eksark, ?)
* [Fildeling](http://filer.uib.no) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=271) filer.uib.no
* [fs-pres.app.uib.no](https://fs-pres.app.uib.no) [📕](https://tk.app.uib.no/node/149) FS-pres [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=324)
* [Grind (drift)](http://www.grind.no/) [📕](https://tk.app.uib.no/node/139) Grind [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=329) Grind
* [Offentlig journal for UiB](https://offjournal.app.uib.no)
* Open Scholar (under avvikling)
* [Personlige og organisatoriske nettsider](http://folk.uib.no)
* [Regelsamlingen](http://regler.app.uib.no) [📕](https://tk.app.uib.no/node/19) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=154)
* [Rominformasjon](http://rom.app.uib.no/romInfo/)
* [Syleguide](http://styleguide.app.uib.no)
* [universitetsmuseet.no](https://universitetsmuseet.no)
* [Utvekslingsrapport](https://studentrapport.app.uib.no) [📕](https://tk.app.uib.no/node/148) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=308) Studentrapport
* [W3](https://w3.uib.no) [📕](https://tk.app.uib.no/node/10) UiBs websider [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=182) Eksternweb [🎯](https://rts.uib.no/projects/w3f) [🗄](https://git.app.uib.no/it/site/w3.uib.no)
* [Webber](http://webber.uib.no)
* Webhotell
* Wiki-tjeneste [📕](https://tk.app.uib.no/node/13) Wiki [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=189) Wiki
* Wordpress-tjeneste [📕](https://tk.app.uib.no/node/14) Wordpress nettside [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=257) Wordpress
* [www.uib.no](https://www.uib.no) [📕](https://tk.app.uib.no/node/10) UiBs websider [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=182) Eksternweb [📃](https://wikihost.uib.no/itwiki/index.php/Eksternweb)

| Techology | Services | People |
|-----------|---------:|-------:|
| php | 12 | 3 |
| pg | 5 | 1 |
| drupal7 | 4 | 2 |
| rhel7 | 4 | 2 |
| mysql | 3 | 1 |
| drupal8 | 2 | 2 |
| python | 2 | 1 |
| django | 2 | 0 |
| nginx | 1 | 1 |
| varnish | 1 | 1 |
| apache | 1 | 0 |
| aws-s3 | 1 | 0 |
| elastic | 1 | 0 |
| ezpublish | 1 | 0 |
| laravel | 1 | 0 |
| mediawiki | 1 | 0 |
| uwsgi | 1 | 0 |
| wordpress | 1 | 0 |
| js | 0 | 1 |
| sql | 0 | 1 |

## Team Språk [🎯](https://rts.uib.no/projects/team3/agile/board)

People (4):

* @egu025 [Eirik T. Gullaksen](https://www.uib.no/user/uib/egu025)
* @lno063 [Lennart Nordgreen](https://www.uib.no/user/uib/lno063)
* @noe021 [Nils Øverås](https://www.uib.no/user/uib/noe021)
* @fnsov [Ole Voldsæter](https://www.uib.no/user/uib/fnsov)

Services (12):

* [Dialektbasen for Sogndal](https://dialektbase-sogndal.uib.no/)
* Gammel redigeringsapplikasjon for ordbøkene
* [Norsk Ordbok](http://no2014.uib.no/perl/ordbok/no2014.cgi)
* [Nynorskordboka og Bokmålsordboka](http://ordbok.uib.no) [📕](https://tk.app.uib.no/node/280) Ordbok
* [oda.uib.no](http://oda.uib.no)
* 🚧 [Ordbank API](https://ordbank-api.testapp.uib.no/) [🗄](https://git.app.uib.no/spraksamlingane/ordbank-api)
* [Ordbok for eksamen](https://omod.uib.no/eksamen)
* [Ordbokshotellet](http://usd.uib.no/perl/search/search.cgi?appid=118&tabid=1777)
* Seddelapplikasjonen
* [Setelarkivet - Norsk ordbok](http://usd.uib.no/perl/search/search.cgi?appid=8&tabid=436)
* [Søk i Termportalen](https://oda.uib.no/app/term)
* [usd.uib.no](http://usd.uib.no)

| Techology | Services | People |
|-----------|---------:|-------:|
| oracle | 4 | 3 |
| python | 4 | 3 |
| sqlite | 3 | 2 |
| pg | 1 | 3 |
| flask | 1 | 1 |
| graphql | 1 | 1 |
| java | 1 | 1 |
| perl5 | 1 | 1 |
| rest | 1 | 1 |
| aws-s3 | 1 | 0 |
| delphi | 1 | 0 |
| tomcat | 1 | 0 |
| windows | 1 | 0 |
| js | 0 | 1 |
| k8s | 0 | 1 |
| mysql | 0 | 1 |
| plsql | 0 | 1 |
| react | 0 | 1 |
| rhel7 | 0 | 1 |

## Teamless

People (3):

* @cdo021 [Chernet Dotche](https://www.uib.no/user/uib/cdo021)
* @gaa041 [Gisle Aas](https://www.uib.no/user/uib/gaa041)
* @nka047 [Nina Kaurel](https://www.uib.no/user/uib/nka047)

Services (27):

* [apidoc.app.uib.no](http://apidoc.app.uib.no) [📕](https://tk.app.uib.no/node/151) APIDoc [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=281) Apidoc
* [apprepo.uib.no/yum](https://apprepo.uib.no/yum) [📃](https://wiki.uib.no/itwiki/index.php/Apprepo.uib.no)
* [Avhandlingsportalen — Bring Your Own Thesis](https://avhandling.uib.no) [🗄](https://git.app.uib.no/uib-ub/phd-portal/byoth)
* [britachat.app.uib.no](https://britachat.app.uib.no)
* [dagstavle.app.uib.no/](http://dagstavle.app.uib.no/)
* Elasticserch [📕](https://tk.app.uib.no/node/247) Elasticsearch [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=321) Search As A Service
* [Fravær](https://fravær.app.uib.no) [📕](https://tk.app.uib.no/node/319) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=243)
* [Gitlab ved UiB](https://git.app.uib.no) [📕](https://tk.app.uib.no/node/322) GitLab CE ved UiB [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=337) git.app.uib.no
* [Gitolite ved UiB](https://git.uib.no) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=233) Git
* Grab Your Own ISBN [🗄](https://git.app.uib.no/uib-ub/phd-portal/groin)
* Jenkins/logging/kodedocs
* [kark.uib.no](http://kark.uib.no)
* [Notat](http://notat.app.uib.no/) [📕](https://tk.app.uib.no/node/199) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=311) Notat.app.uib.no
* [overvåkning.app.uib.no](http://overvåkning.app.uib.no)
* [Prosjektverktøy Redmine](http://sak.uib.no) [📕](https://tk.app.uib.no/node/36) Sakshåndteringsverktøy Redmine
* [pycode.app.uib.no/](https://pycode.app.uib.no/)
* [RTS](https://rts.uib.no) [📕](https://tk.app.uib.no/node/152) Prosjektverktøy Redmine (RTS) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=221) Prosjektverktøy Redmine
* [Sentralt loggmottak](https://kibana.uib.no)
* [shared.app.uib.no](http://shared.app.uib.no)
* [Skiltdatabasen](https://skiltdb.app.uib.no/) [📕](https://tk.app.uib.no/node/135) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=280)
* [skjemaker.app.uib.no](https://skjemaker.app.uib.no) [📕](https://tk.app.uib.no/node/29) Webskjema (Skjemaker) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=241) Skjemaker
* [stats.uib.no](https://stats.uib.no)
* [Telefonifakturering](https://https://tlfabb.app.uib.no) [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=300) Tlfabb
* [Tjenestekatalogen](https://tk.app.uib.no) [📕](https://tk.app.uib.no/node/288)
* [Tjenestestatus (oppetid)](http://oppetid.app.uib.no)
* Tps-pakker
* Webstatistikk – Piwik [⚙️](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=246) Webstatistikk-Piwik

| Techology | Services | People |
|-----------|---------:|-------:|
| php | 7 | 1 |
| pg | 5 | 0 |
| js | 3 | 0 |
| node | 3 | 0 |
| django | 2 | 1 |
| python | 2 | 1 |
| elastic | 2 | 0 |
| k8s | 2 | 0 |
| nginx | 2 | 0 |
| rails | 2 | 0 |
| redmine | 2 | 0 |
| sqlite | 2 | 0 |
| drupal7 | 1 | 1 |
| apache | 1 | 0 |
| gitlab | 1 | 0 |
| kibana | 1 | 0 |
| laravel | 1 | 0 |
| machform | 1 | 0 |
| rhel6 | 1 | 0 |
| rhel7 | 1 | 0 |
| uwsgi | 1 | 0 |
| excel | 0 | 1 |
| git | 0 | 1 |
| graphql | 0 | 1 |
| perl5 | 0 | 1 |
| sharepoint | 0 | 1 |

## Other services from Tk (26)

* [Aktivering av Feide-tjenester](http://tk.app.uib.no/node/156) owner:cdo021 operator:cdo021 operator:nka047
* [Bergen Open Access Publishing](http://tk.app.uib.no/node/140) operator:lno063 operator:mihho team:Team Publisering
* [Bergen Open Research Archive (BORA)](http://tk.app.uib.no/node/84) operator:lno063 operator:mihho team:Team Publisering
* [Besvarelsesrepo](http://tk.app.uib.no/node/508) operator:aar028 operator:edpck team:Team Lærings- og forskningsstøtte
* [Bygg og rominfo](http://tk.app.uib.no/node/499) operator:mme045 team:Team Publisering
* [Digital skoleeksamen på studentens eget utstyr](http://tk.app.uib.no/node/97) team:Team Lærings- og forskningsstøtte
* [Digitalt (del av UBs spesialsamlinger)](http://tk.app.uib.no/node/161) operator:lno063 operator:mihho team:Team Publisering
* [EKT databasen](http://tk.app.uib.no/node/108) operator:mihho
* [FDrest](http://tk.app.uib.no/node/498) owner:cdo021 operator:edpck operator:mme045 team:Team Lærings- og forskningsstøtte
* [Fakturabehandlingssystem (Basware IP)](http://tk.app.uib.no/node/88) operator:edprf operator:ibe085 team:Team Administrative systemer
* [Feide-berettiget](http://tk.app.uib.no/node/155) owner:cdo021 operator:cdo021 operator:nka047
* [Frode (Lukket arkiv CRIStin)](http://tk.app.uib.no/node/160) operator:lno063 operator:mihho
* [MariaDB hotell](http://tk.app.uib.no/node/286) owner:mihho operator:lno063 operator:mihho operator:mme045 team:Team Publisering
* [MasterArk](http://tk.app.uib.no/node/287) operator:lno063 operator:mihho operator:tle029 team:Team Lærings- og forskningsstøtte
* [Målinger og statistikk av webtjenester](http://tk.app.uib.no/node/507) operator:apa033 operator:mihho operator:mme045 team:Team Publisering
* [Nordplusmusic - søknadssystem](http://tk.app.uib.no/node/138) owner:edprf operator:bubjb team:Team Administrative systemer
* [Passord (skifte av passord) på brukerkonto ved UiB](http://tk.app.uib.no/node/129) owner:cdo021 operator:edpeb operator:ibe085 operator:jbi022 operator:mihho team:Team Masterdata, Identitet og Tilgang
* [RPA og automatiserte prosesser ](http://tk.app.uib.no/node/501) operator:ttr031 team:Team Administrative systemer
* [Søknadsweb for etter- og videreutdanning](http://tk.app.uib.no/node/114) operator:edprf team:Team Administrative systemer
* [Søknadsweb](http://tk.app.uib.no/node/113) team:Team Administrative systemer
* [Tilgjengeliggjøring av tjenester via Dataporten](http://tk.app.uib.no/node/284) owner:cdo021 operator:cdo021 operator:jbi022 operator:lno063
* [Tilgjengeliggjøring av tjenester via Feide](http://tk.app.uib.no/node/154) owner:cdo021 operator:cdo021
* [Tjenesteovervåking (Tjenestestatus)](http://tk.app.uib.no/node/500) owner:cdo021 operator:mme045 team:Team Publisering
* [Trådlaus AV over Wifi (Casting)](http://tk.app.uib.no/node/467) operator:amy098
* [Universitetsmuseet i Bergen](http://tk.app.uib.no/node/505) operator:jhe052 team:Team Publisering
* [Webhotell](http://tk.app.uib.no/node/506) owner:mihho operator:lno063 operator:mihho operator:mme045 team:Team Publisering

## Other services from CMDB (81)

* [Basware](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=40)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Besvarelsesrepo](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=334)  ansv_seksjon=IT-avd, App - Alle saker
* [Billedbasen ved UB](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=185)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [BORA](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=25)  ansv_seksjon=IT-avd, App - Alle saker
* [britachat](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=342)  ansv_seksjon=IT-avd, App - Alle saker
* [BSRS](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=231)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [CAIM-Databasen](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=144)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Canvas Pilot](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=309)  ansv_seksjon=IT-avd, App - Alle saker
* [Clarino](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=335)  ansv_seksjon=IT-avd, App - Alle saker
* [Colon](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=328)  ansv_seksjon=IT-avd, App - Alle saker
* [Coursekeeper](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=53)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [DigEks](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=242)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Dikult](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=332)  ansv_seksjon=IT-avd, App - Alle saker
* [Doculive](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=35)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [DOFI](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=130)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Egendriftet Løsning](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=277)  ansv_seksjon=IT-avd, App - Alle saker
* [Eksamensarkivet](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=103)  ansv_seksjon=IT-avd, App - Alle saker
* [Ekt](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=212)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Etter- og videreutdanningskatalogen](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=129)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [EVUweb](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=120)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [FDrest](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=206)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Frode](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=269)  ansv_seksjon=IT-avd, App - Alle saker
* [Griegakademiet - Følgeskjema til søknad](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=296)  ansv_seksjon=IT-avd, App - Apex
* [Historisk økonomisystem](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=37)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [HMS-rapportering](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=201)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Integrasjon mot ID-porten](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=297)  ansv_seksjon=IT-avd, App - Alle saker
* [Internfakturering IT-avdelingen](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=188)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [J4LFopserver](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=232)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Jenkins](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=234)  ansv_seksjon=IT-avd, App - Viderebehandling
* [Laban(NOKLUS)](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=54)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [LDAP - FEIDE/Moria](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=220)  ansv_seksjon=IT-avd, App - Sebra
* [LiveCode Mysql](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=253)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [MariaDB](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=236)  ansv_seksjon=IT-avd, App - Alle saker
* [MasterArk](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=327)  ansv_seksjon=IT-avd, App - Alle saker
* [Meldingstjenesten](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=102)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Meltzer-fond og Bergen universitetsfond](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=180)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Miljøpulsen](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=217)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Nordplus søknadssystem](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=307)  ansv_seksjon=IT-avd, App - Apex
* [nsd.uib.no](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=354)  hoved_ansv=Anders Paulsen
* [Open Journal Systems](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=285)  ansv_seksjon=IT-avd, App - Alle saker
* [Ordbok-backend](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=326)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Ordbok-frontend](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=325)  ansv_seksjon=IT-avd, App - Alle saker
* [pg-app](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=343)  ansv_seksjon=IT-avd, App - Fagområdet åpne systemer
* [pg-vle](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=344)  ansv_seksjon=IT-avd, App - Fagområdet åpne systemer
* [pg-w3](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=345)  ansv_seksjon=IT-avd, App - Fagområdet åpne systemer
* [postgresql](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=247)  ansv_seksjon=IT-avd, App - Viderebehandling
* [Primus](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=145)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [print-faktura](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=331)  ansv_seksjon=IT-avd, App - Alle saker
* [Prosjektdatabase](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=58)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Prosjektportalen](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=351)  ansv_seksjon=IT-avd, App - Alle saker
* [Prosjektstyring](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=208)  ansv_seksjon=IT-avd, App - Viderebehandling
* [På Høyden](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=283)  ansv_seksjon=IT-avd, App - Alle saker
* [Påmelding, doktorpromosjon](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=216)  ansv_seksjon=IT-avd, App - Viderebehandling
* [Påmeldingssystem](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=184)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Redigere driftsmeldinger på Mi side](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=290)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Retorikkdatabasen](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=59)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [rman catalog](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=202)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Rombooking](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=42)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [SLP](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=36)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Studentdatabaser](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=125)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Syllabus ekstern visning](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=186)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Søknadsskjema, sommerkurs LLE](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=214)  ansv_seksjon=IT-avd, App - Viderebehandling
* [Søknadssystem for fond og legater, søknadsapplikasjon](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=292)  ansv_seksjon=IT-avd, App - Apex
* [Søknadssystem for fond og legater, tildelingsapplikasjon](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=293)  ansv_seksjon=IT-avd, App - Apex
* [Søknadssystem for utenlandske søkere](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=133)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Søknadsweb](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=24)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Test MiSide](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=310)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [TEST-basware](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=197)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [TEST-Rombooking](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=75)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [TEST-Sebra](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=77)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [TEST-Studweb](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=47)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Test-økonomirapportering](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=276)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [TOAD](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=204)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [ttscreens](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=213)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [Utvekslingsavtaler](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=264)  ansv_seksjon=IT-avd, App - Alle saker
* [Verneområder](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=227)  ansv_seksjon=IT-avd, App - Fagområdet webutvikling
* [videoarkiv.dig.uib.no](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=306)  ansv_seksjon=IT-avd, App - Alle saker
* [Web-discoverer](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=49)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [wu-overvåkning](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=323)  hoved_ansv=Cato Kolås
* [Økonomi-avstemming](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=76)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system
* [Økonomirapportering](https://bs.uib.no/?module=cmdb&action=viewt&tjeneste=23)  ansv_seksjon=IT-avd, App - Fagområdet Oracle, adm.system

# Technologies

## 01 Application

*  [Excel](https://en.wikipedia.org/wiki/Microsoft_Excel)   🤓
*  [Gitlab CE](https://gitlab.com/gitlab-org/gitlab-ce/)   💈
*  [Kibana](https://www.elastic.co/products/kibana)   💈
*  [MachForm](https://www.machform.com)   💈
*  [Redmine](https://www.redmine.org)   💈💈

## 02 Content Management System

*  [Drupal 7](https://www.drupal.org/7)   💈💈💈💈💈🤓🤓🤓
*  [Drupal 8](https://www.drupal.org/8)   💈💈🤓🤓
*  [Mediawiki](https://www.mediawiki.org)   💈
*  [Sharepoint](https://products.office.com/en-us/sharepoint/collaboration)   💈💈🤓🤓
*  [Wordpress](https://wordpress.org)   💈
*  [eZ Publish](https://doc.ez.no/eZ-Publish)   💈

## 03 Web Framework

*  [APEX](https://apex.oracle.com/)   💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈🤓🤓🤓🤓
*  [Bootstrap](https://getbootstrap.com)   💈
*  [Django](https://www.djangoproject.com)   💈💈💈💈🤓
*  [Flask](http://flask.pocoo.org)   💈🤓
*  [Laravel](https://laravel.com)   💈💈💈💈💈💈🤓
*  [Rails](https://rubyonrails.org)   💈💈💈😶
*  [React](https://reactjs.org)   😶
*  [Symfony](https://symfony.com)   
*  [Vue](https://vuejs.org)   💈

## 04 Programming language

*  [CSS](https://no.wikipedia.org/wiki/Cascading_Style_Sheets)   
*  [Delphi](https://en.wikipedia.org/wiki/Delphi_(IDE))   💈
*  [Java](https://en.wikipedia.org/wiki/Java_(programming_language))   💈🤓
*  [JavaScript](https://en.wikipedia.org/wiki/JavaScript)   💈💈💈💈💈💈🤓🤓🤓
*  [PHP](http://www.php.net)   💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈🤓🤓🤓🤓🤓🤓🤓🤓🤓🤓
*  [PL/SQL](https://en.wikipedia.org/wiki/PL/SQL)   🤓🤓
*  [Perl 5](https://www.perl.org)   💈🤓🤓🤓
*  [PowerShell](https://docs.microsoft.com/en-us/powershell/)   😶
*  [Python](https://python.org)   💈💈💈💈💈💈💈💈💈💈💈😶🤓🤓🤓🤓🤓
*  [Ruby](https://www.ruby-lang.org/)   💈😶
*  [SQL](https://en.wikipedia.org/wiki/SQL)   🤓🤓🤓
*  [TypeScript](https://www.typescriptlang.org)   

## 05 Database

*  [AWS Aurora](https://aws.amazon.com/rds/aurora/)   
*  [AWS S3](https://aws.amazon.com/s3/)   💈💈
*  [Azure BLOB](https://azure.microsoft.com/nb-no/services/storage/blobs/)   
*  [Elastic search](https://en.wikipedia.org/wiki/Elasticsearch)   💈💈💈💈
*  [Microsoft SQL](https://en.wikipedia.org/wiki/Microsoft_SQL_Server)   🤓
*  [MySQL and MariaDB](https://en.wikipedia.org/wiki/MySQL)   💈💈💈💈🤓🤓🤓🤓
*  [Oracle DB](https://www.oracle.com/database/)   💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈💈😶😶🤓🤓🤓🤓🤓🤓
*  [PostgreSQL](https://www.postgresql.org)   💈💈💈💈💈💈💈💈💈💈💈💈🤓🤓🤓🤓🤓
*  [Redis](https://redis.io)   
*  [SQLite](https://www.sqlite.org/)   💈💈💈💈💈💈🤓🤓

## 06 Web API

*  GraphQL   💈🤓
*  REST   💈🤓

## 07 Web server

*  [Apache](https://httpd.apache.org)   💈💈
*  [Nginx](https://nginx.org)   💈💈💈🤓
*  [Varnish](https://varnish-cache.org)   💈🤓

## 08 Runtime environment

*  [AWS Lambda](https://aws.amazon.com/lambda/)   
*  [Blue Prism](https://www.blueprism.com)   💈😶🤓🤓
*  [Digiforms Server](https://digiforms.no/htmlViewer?templateName=server&documentName=platform)   💈
*  [Kubernetes](https://kubernetes.io)   💈💈🤓🤓🤓
*  [Node](https://nodejs.org/)   💈💈💈
*  [Redhat Enterprise Linux 6](https://en.wikipedia.org/wiki/Red_Hat_Enterprise_Linux#RHEL_6)   💈
*  [Redhat Enterprise Linux 7 (or CentOS)](https://www.centos.org)   💈💈💈💈💈🤓🤓🤓🤓
*  [Rocket](https://pythonhosted.org/rocket/)   💈
*  [Tomcat](http://tomcat.apache.org)   💈
*  [Uwsgi](https://uwsgi-docs.readthedocs.io/en/latest/)   💈💈
*  [Watson](https://www.ibm.com/watson)   💈
*  [Windows](https://no.wikipedia.org/wiki/Microsoft_Windows)   💈💈

## 09 Tool

*  [Git](https://git-scm.com)   🤓
*  [TopDesk](http://topdesk.com)   💈

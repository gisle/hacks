all: app-tech.md

app-tech.md: app-tech app-tech.yaml tk-services.yaml
	pipenv run ./app-tech >$@ || rm -f $@

reset:
	git checkout -f app-tech.md

clean:
	rm app-tech.md

update-tk:
	curl -s https://tk.app.uib.no/api/v1/service/all | jq | yq read -P - >tk-services.yaml

update-bs:
	(cd bs && ./update)

Dette er et lite notat som prøver å utforske hva det teknisk vil si og prøve å
følge innføringsplanen som ble skissert i prosjektmøtet 2019-09-12. Ordet _IGA_
er "placeholder" for det nye systemet vi skal innføre.

## Trinn 1.0: Sette opp IGA

Den nye IGA-applikasjonen settes opp og UiB får en "tenant" i løsningen.  Dette
oppsettet inkluderer:

- Innlogging via FEIDE.
- Strukturer for å kunne representere organisasjon, personer og konti.
- Manuelt oppsett av noen brukere og roller for administrativ tilgang til IGA.

## Trinn 1.1: Koble til SEBRA som datakilde for IGA

Sørge for at IGAs datamodell oppdateres med organisasjon, personer og konti fra
SEBRA.  Dette er en temporær løsning og alle skitne triks for synkroniseringen
kan brukes (f.eks.  direkte sugerør rett inn i SEBRAs Oracle database hvis
formålstjenelig). I utgangspunktet så må all informasjon som skal videre til
LDAP overføres.

Vi satser på at ikke SEBRA må endres, men det mulig at det er formålstjenelig å
sette opp vask og eksporter av data fra SEBRA.

Åpne punkter:

- Er det noe data vi mangler?
- Flytter vi over all historisk personinfo fra SEBRA, også for deaktiverte konti. Legger
  GDPR noen føringer.
- Håndtere kurskonto som en underkonto av person 

## Trinn 1.2: Hente masterdata fra FS til IGA

Ikke personene direkte (siden disse kommer fra SEBRA), men deres relasjon til
studieprogram og organisasjon (institutt). Event-drevet synkronisering med
UH:IntArk vil være ønskelig, men det er kanskje ikke tilgjengelig så tidlig.

## Trinn 1.3: Synkronisere personer og organisasjon i ldap.uib.no fra IGA

Her er målet at vi flytter oppdateringen av ldap.uib.no fra SEBRA til IGA.
AD er uberørt og syncer fortsatt utvalgte person-attributter fra LDAP.

Hvis vi ønsker å endre på hvordan ting representeres i LDAP er dette et godt
tidspunkt å gjennomføre den endringen, men vi må sørge for at FEIDE-pålogging
og andre tjenester som avhenger av LDAP fortsetter å fungere.

Endringer:

- SEBRA slutter å oppdatere person-treet (ou=people,dc=uib,dc=no) og organisasjons-treet (ou=organization,dc=uib,dc=no) i ldap.uib.no
- IGA begynner å oppdatere organisasjons-treet (ou=organization,dc=uib,dc=no) i ldap.uib.no
- IGA begynner å oppdatere person-treet (ou=people,dc=uib,dc=no) i ldap.uib.no
- IGA overvåker ldap.uib.no og plukker opp manuelle endringer og håndterer disse
- IGA skriver LDAP gruppene til AD

Åpne punkter:

- Flyt av passord? Passordendringen skjer fremdeles i SEBRA, men sannsynligvis må også klartekstpassordet flyte til IGA.
- Kurskontoer må være med (de er i person-treet, men har ikke klasse `norEduPerson`.
- IGA skriver LDAP gruppene til AD fordi gruppedata i SEBRA ikke videreføres. Er det lurt?
- Håndteringen av 'manuelle' endringer i LDAP kan være at IGA bare overskriver de aktuelle subtrærne (samme som SEBRA gjør idag)
- Randsone-organisasjoner (nsd,...)

## Trinn 1.4: Flytte gruppehåndteringen i ldap fra SEBRA til IGA

Endringer:

- SEBRA slutter å oppdatere grupper i ldap.uib.no
- IGA importerer gruppene fra ldap.uib.no
- Sette opp automatiserte grupper basert på masterdata fra FS (hvilke studieprogram/emne er studentene meldt opp i)
- Manuelt vedlikehold av grupper implementeres i IGA (data om eierskap av
  gruppene er kanskje ikke i ldap og må importeres fra SEBRA som en
  engangsjobb?)
- IGA overvåker også manuelle endringer av grupper

Åpne punkter:

- Skal skillet mellom Unixgrupper og Unixnetgroup videreføres
- Skriver SEBRA-gruppedata direkte til AD? `OU=ldap,OU=SpesialGrupper,DC=uib,DC=no`

## Trinn 1.5: Synkronisere AD-brukere fra IGA

Her er målet å flytte onboarding/offboarding og oppdatering av brukere i AD til
IGA.

Synkroniseringen fra AD til Azure AD er uberørt og fortsetter å kjøre som før.

Endringer:

- SEBRA slutter å kommunisere med AD (kommandoer som sendes for å lage nye brukere++) og koden på Windows-siden pensjoneres
- AD-sync av attributter fra LDAP slås av
- IGA oppdaterer AD direkte basert på en standard connector

Hvis vi ønsker å endre på hvordan ting representeres i AD er dette et godt
tidspunkt å gjennomføre den endringen, men vi må sørge for at tjenester som
avhenger av AD fortsetter å fungere.

Åpne punkter:

- _What could possibly go wrong?_
- Treestrukturen kan vi kanskje ikke klare å lage?

## Trinn 1.6: Vedlikehold av AD-grupper i IGA

Her er målet å flytte vedlikeholdet av grupper ut av AD slik at prosessene
styres av regler i IGAet.

Endringer:

- Gruppene importeres fra AD til IGA
- Manuelt vedlikehold av gruppene overføres til IGA
- IGA overvåker gruppene
- Automatiske regler for populering og oppdatering av gruppene etableres der
  det gir mening og vi allerede har nok data

Åpne punkter:

- Vi kan redusere scope ved å utsette hele dette punktet. Det er ikke så mye som teknisk sett avhenger at vi har
fått kontroll over AD-gruppene våre.

## Trinn 1.7: Synkronisere Azure AD fra IGA

Målet er at IGAet skal oppnå oversikt over det som skjer i Azure AD og
at kontrollen over IAM-prosessene flyttes til IGA.

Endringer:

- Slår av synken mellom AD og Azure AD (AD Connect)
- Azure AD oppdates fra IGA
- IGA overvåker Azure AD (brukere, grupper)
- IGA plukker opp gjestebrukere som registrerer seg i Azure AD

## Trinn 2: Koble FS til IGA

Målet her er å begynne konsumeringen av masterdata fra FS direkte til IGAet og
bruke dette til å styre tilganger til personer som studenter og lærere.
Identitetene flyter fremdeles via SEBRA siden disse må samordnes med data
fra Paga.

Åpne punkter:

- Uklart hva som kan gjøres her
- Kunne vært brukt til å flytte tillgangstyring i FS til IGA

## Prep 3.1: Koble DFØ fiktivt firma til IGA

Målet her er å etablere koblingen mellom DFØ og IGA i henhold til UH:IntArk og
planene som er skissert i IPAS-arbeidet med DFØ. Personer og organisasjon som
blir hentet fra DFØ beskriver et fiktivt firma og er testmiljøet til
BOTT-ØL-prosjektet.

## Trinn 3: Koble PAGA/FS av SEBRA og DFØ/FS til IGA

Målet her er å flytte kontogenereringen til IGA og ut av SEBRA, mens tilstanden
i SEBRA må holdes i sync.  Dette samtidig som Paga fases ut og DFØ fases inn.
Dette er en veldig omfattende omlegging.

Endringer:

- Slå av import av PAGA og FS data i SEBRA
- Importere studenter fra FS til IGA
- Importere ansatte og gjester fra DFØ til IGA
- Oppretting av personer og kontoer i IGA. Flytte over workflow for setting av passord.
- Passordendring og recovery flyttes fra SEBRA til IGA
- Sette opp synkronisering av personer og brukere fra SEBRA til IGA

Åpne punkter:

- Omfattende omlegging i SEBRA for å kunne hente organisasjon og personer fra IGA. Er det gjennomførbart?
- Godkjennere mister jobben sin

# Prosess for gjesters livssyklus

En _gjest_ er en person som skal ha en relasjon til universitetet, men som ikke
faller under kategorien _fast ansatt_ eller _student_.

Målet med prosessen er å få registrert en gjest slik at identifikasjon sikres
tilstrekkelig og vi også fanger opp gjestens relasjon til universitetet over
tid.  Gjesten kan være kjent fra før. Hva som er tilstrekkelig identifikasjon
avhenger av hvilken tilknytting som ønskes.  Tilknyttinger som medfører
utstedelse av universitetskonto krever _sikker identifikasjon_.

Gjestens _sponsor_ er en person med eksisterende tilknytting til universitetet
som ønsker at relasjonen oppstår.  For noen tilknyttinger må _sponsor_ få
godkjenning av _bemyndiget_ før tilknyttingen blir effektiv.

Prosessen er som følger:

1. Sponsor innhenter informasjon om personen som sponsor vil skal oppnå en relasjon til universitetet.
   Minstekrav er en epost-adresse eller et SMS-nummer.

2. Sponsor skriver inn epost-adressen i et invitasjonsskjema.  Her må det også
   registreres hvilken relasjon som ønskes og tidsrommet den skal gjelde for.
   Hvis sponsor har bekreftet identiteten kan det krysses av for det og fødselsnummer eller passnummer opplyses.
   Sponsor kan også søke etter tidligere registrering i HR-systemet for gjesten og referere til denne.

3. Gjest mottar en melding fra universitetet med invitasjon og instruksjoner for å identifisere seg.
   Lenken er unik for denne invitasjonen.
   Gjesten bekrefter at relasjonen ønskes ved å følge lenken (verifiserer samtidig epost-adressen eller SMS-nummer)

4. Hvis relasjonen krever sikker identifikasjon har gjesten følgende opsjoner:
       - identifisere seg med minID.
       - sponsor har krysset av for at legitimasjon er framvist og fødselsnummer er oppgitt i (2).
       - ta kontakt med sponsor for å fremlegge legitimasjon

5. Hvis gjesten var kjent fra før bekrefter gjesten at opplysninger, som vi har, slike som navn, fødselsdato, bilde osv. er korrekte.

6. Hvis ikke gjesten var kjent fra før kan gjesten selv oppgi sine personalia; navn, fødselsdata, bilde osv.

7. Hvis tilknyttingen krevet godkjenning sendes det er forspørsel til bemyndiget.

8. Bemyndiget avslår eller innvilger tilknyttingen.  Melding om avslag går til
   sponsor. Sponsor er ansvarlig for videre kommunikasjon med gjest, eventuelt å
   overbevise bemyndiget.

9. Tilknyttingen aktiveres ved start-tidspunktet.  Hvis dette medfører utstedelse av ny konto starter prosessen _kontoaktivering_.

10. Når slutttidspunktet nærmer seg får gjesten og sponsor melding om det.

11. Sponsor kan velge å forlenge tilknyttingen eller registrere en ny tilknytting.

12. Når slutttidspunktet inntreffer deaktiveres tilknytttingen.

Personer kan ha flere overlappende tilknyttinger.  Dette må tas hensyn til når man beskriver hva som skal skje
ved aktivering eller deaktivering av tilknyttinger.

![BPMN diagram av prosessen](Gjesteprosess.png)

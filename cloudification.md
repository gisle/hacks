# Prosjekt: Cloudification

## Todo

- [x] Velg prosjekt-tracker --> <https://rts.uib.no/projects/w3cloud>
- [x] Definer opp arbeidspakker
- [ ] Responder på prosjektplanen v0.2 til Arnfinn
- [x] Kall inn til møter før ferien
- [x] Lag presentasjon om Docker

## Meetings

### 2018-09-05 Workshop med Microsoft

### 2018-07-02 Heldags workshop with ATEA om Azure Governance

Erlend Magne Buås-Hansen presenterte standard slides som Atea har på temaet.
Diskusjon om navnestandard og hvordan sette opp policy og kostanalyse.

Tilstede: Arnfinn, Gisle, Raymond, Are, Rannveig, Mike, Lennart, Ana og Amund.

### 2018-07-01 Status med Arnfinn

Tilstede: Gisle og Arnfinn

### 2018-06-26 Status med Arnfinn

Tilstede: Gisle og Arnfinn

### 2018-06-26 Prosjektstatusmøte

Tilstede: Gisle, Are, Ana, Lennart og Raymond.

Snakket litt mer om dockerworkshopen og hvordan vi kunne sette opp image for w3 applikasjonen.
Review av kostnader på Azure.
Snakket om planene for workshops med Atea neste uke.

### 2018-06-25 Docker workshop

Tilstede: Gisle, Are, Amund, Ana, Raymond, Lennart, Mike

Gisle presenterte og demonstrerte hva docker er. Laget også container registry i Azure og deployet
"official" drupal fra Docker Hub i Azure.

### 2018-06-19 Prosjektstatusmøte

Tilstede: Gisle, Are, Ana, Raymond, Lennart, Mike, Kristian, Christoffer

### 2018-06-18 Scoping call with Microsoft

Attending: Microsoft (Håvard Haukeberg, Maria Trollvik, Anthony Bitar), Arnfinn and Gisle

Decided on the workshop in september.  Before that we should write up some documentation
of the services we want to move.

### 2018-06-08 Intern kick-off hos UiB

## Resources

* [Azure Portal](https://portal.azure.com)
* [RTS prosjektet](https://rts.uib.no/projects/w3cloud)
* [SharePoint](https://universityofbergen.sharepoint.com/sites/Cloudification)
* [MS Teams](https://teams.microsoft.com/l/team/19%3a6b733ff53bd1486fac73fe22f3d5699f%40thread.skype/conversations?groupId=1f28a12b-4fa9-4fb4-a150-ebb0ca8e3805&tenantId=648a24bc-a98d-4025-9c60-48c19a142069)
* [Arnfinns prosjektdokumentasjon](https://unitdir-my.sharepoint.com/personal/arnfinna_unit_no/Documents/Forms/All.aspx?slrid=a01a6f9e%2D70c2%2D0000%2D1788%2De0c4a916846c&RootFolder=%2Fpersonal%2Farnfinna%5Funit%5Fno%2FDocuments%2F03%20Prosjekter%2F10%20Prosjekt%20Cloudification%20UiB&FolderCTID=0x0120002C4165A3B7CEAD4DA2CEF3CC99C85FEC)

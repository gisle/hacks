# Organa — UiBs DevOps-plattform for applikasjoner

_Organa_ er tenkt som en plattform for å kunne produksjonsette, drifte, teste og
utvikle applikasjoner på en god og moderne måte.

Plattformen skal bestå av tjenester, rutiner, konvensjoner, dokumentasjon og
praksiser som støtter arbeidet til team som forvalter en
applikasjonsportefølje.

Plattformen er etablert ut fra behovene vi opplevde i Seksjon for
Applikasjoner ved UiBs IT-avdeling, og er delvis preget av praksiser og
verktøy som er blitt innarbeidet hos oss. Plattformen vil ikke være begrenset
til seksjonens portefølje når den blir operativ.

Plattformen skal støtte applikasjonsdeployment både i skyen og på maskiner i
vårt eget datasenter.

## Applikasjon

Hva mener vi med _applikasjon_ i Organa?

Dette er et begrep som dekker mange ting; eksempler kan være web-applikasjoner,
mikrotjenester og integrasjonskomponenter.  For web-tjenester så er det ofte
domenenavnet som definerer applikasjonen. I andre tilfeller er det tjenesten
som er enheten som omfattes.

En applikasjon er gitt et navn og realiseres av kode som implementerer et
avgrenset system. Koden til en applikasjon ligger i sitt eget git-repo. En
applikasjon tilbyr en tjeneste og kan bygge på andre underliggende tjenester
(f.eks.  databaser eller spesialiserte mikrotjenester av ulike slag).  Den
enkleste applikasjon man kan tenke seg er et statisk nettsted.

Applikasjoner kan instansieres.  Vi bruker det blandt annet til å kjøre
separate test- og staging-instanser av applikasjonen.

## Git

Git er versjonskontrollsystemet som vi har valgt å bruke.  Vi bruker allerede
git til applikasjonskoden vår og delvis til konfigurasjon.  Vi har bygd opp
noen arbeidsflyter basert på [UiBs GitLab
installasjon](https://git.app.uib.no).  Den tjenesten er også tilgjengelig for
brukere utenfor UiB. Vi regner med at dette blir en sentral komponent i Organa.

Organa bør prøve å komme så nær opp til filosofien til
[git-ops](https://www.gitops.tech) som mulig.  Kilden til alt vi gjør skal være
å finne i git, og endringer og deployment skjer ved push til git.

"Alt i git" gjelder selvsagt også kode, oppsett og dokumentasjon som implementer
selve plattformen Organa.

## MVP

Det er ofte klargjørende å jobbe mot et første minste brukbare produkt
([MVP](https://no.wikipedia.org/wiki/Minste_brukbare_produkt)). Hva er det
miste funksjonaliteten som må på plass før vi kan slippe brukerene løs og
begynne å motta feedback?
Brukere her er utviklere og applikasjonsforvaltere.

Dette bør være en tidlig milepæl i prosjektet å få
produksjonsatt MVPen av plattformen.  Etter det jobber vi videre med å utvide
funksjonaliteten og brukbarheten i produktet/plattformen.

Her er et forslag til funksjonalitet i Organa-MVP:

* Organa må kunne deploye en enkel PHP-applikasjon som bruker Postgres direkte
  fra GitLab
* Organa må kunne beskrive deployment både i AWS og i eget datasenter
* Organa må kunne skille på staging og produksjon
* Organa må kunne kjøre unit-tester definert i applikasjonen
* Organa må sette opp DNS-navn og SSL-sertifikat
* Organa må inneholde dokumentasjon for hva som forventes av en utvikler for at
  koden skal være "deployable".
* Organa må inneholde dokumentasjon på hvordan man enkelt deployer i AWS.
* Organa må kunne vises status på instansene som kjører.
* Organa må samle opp loggene og gi tall på bruk av applikasjonen og hvordan
  den oppfører seg.

Dette ble jo allerde en hel del funksjonalitet. Det er kanskje noe i kan strykes
fra listen?

## Krav til plattformen

Her er noen generelle (men enda nokså vage) kvalitetskrav som vi mener at
Organa må oppfylle allerede fra MVP:

* Det skal være enkelt å produksjonsette en enkel applikasjon
* Nye utviklere skal lett komme i gang
* Et team må kunne få god oversikt over sine egne kjørende applikasjoner

## Kubernetes og kontainere

Kontainere og Kubernetes er mye brukt i denne type plattformer (se f.eks.
[nais.io](https://nais.io)).  Det finnes mange ferdigbygde applikasjoner som er
tilgjengelige som kontainer-images. Det vil være praktisk om Organa etterhvert
før støtte til å kjøre disse.

Det er ingenting i MVPen beskrevet over som krever at man pakker applikasjonen
i kontainere og f.eks. bruker Kubernetes som run-time miljø.  Siden et mål er
å  kunne kjøre applikasjoner både on-prem og i skyen så kan Kubernetes være en
høvelig abstraksjon å jobbe ut fra. Dette må prosjektet ta stilling til.


## FaaS (f.eks. AWS Lambda)

[Serverless](https://martinfowler.com/articles/serverless.html) er også
moderene og populært.  Vi bør vurdere hvordan det kan integreres i plattformen.



# Tjenesten UiB:IntArk

UiO vil levere BOTT:IntArk til oss fra Januar 2020.
Dette skal blandt annet tas i bruk for å realisere integrasjonene mot DFØ (BOTT-ØL) og
andre fellestjenester.

Det UiO etter sigende skal levere er UiBs instans av en driftet API gateway basert på
[Gravitee](https://gravitee.io) og en [Rabbit MQ](https://www.rabbitmq.com)
server-instans for UiB.

API-gatewayen vil ta navnet `api.uib.no`.  Køen vil opererer under navnet
`mq.api.uib.no`.  (Ser ut som UiO insisterer på navnene
`api-uib.intark.uh-it.no` for portalen til gatewayen,
`gw-uib.prod.intark.uh-it.no` for gatewayen, og `mq-uib.intark.uh-it.no` for
MQ-serveren).

> Tjenesten er pr 2020-01-10 produksjonsatt fra UiO sin side, men de har enda
> ikke fått kommunisert noe om tjenesten så vi har fått sett og tatt på den.  Vi
> vet derfor strengt tatt ikke om det de leverer er noe som helt funger for oss.

> Fikk litt info om oppsettet 2020-01-14 og aksess til portalen.

Vi må utpeke en lokal system-eier for denne tjenesten.  Det vil være noen
forvaltningsoppgaver og oppsett som UiB selv må administrere.  Tjenesten
registreres i Tjenestekatalogen.  En del av forvaltningsoppgavene er å
samarbeide om oppsettet med de andre universitetene og dokumentere policy for
bruk av tjenesten.

Systemeiere for andre tjenester ved UiB kan registrere APIene sine på
api.uib.no og har deretter i oppgave å forvalte tilgang for sine APIer.  Det
bør da også stenges for direkte aksess APIene som ikke går gjennom
'api.uib.no'.

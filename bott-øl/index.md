## Dokumentarkiv

<https://studntnu.sharepoint.com/sites/prosjektportal-sektor/bott-okonomi-og-hr/Dokumenterpen>

## IPAS – Integrasjon, Plattform, Arkitektur og Sikkerhet

| ID | Navn | Doc | UiB forankring |
|----|------|-----|----|
| 1.8 | **Integrasjoner** | - | - |
| 1.8.0 | Integrasjoner utvikling | | Øystein |
| 1.8.1 | Fremtidig integrasjonsplattform | | Gisle |
| 1.8.2 | Øvrige integrasjoner | | Øystein |
| 1.9 | **Arkitektur** | - | - |
| 1.9.1 | Prinsipper, føringer og modeller | 0.75 | Gisle |
| 1.9.2 | Forretningsarkitektur | 0.2 | Gisle |
| 1.9.3 | Applikasjonsarkitektur | 0.5 | Øystein |
| 1.9.4 | Informasjonsarkitektur og masterdata | | Øystein |
| 1.9.5 | Integrasjonsarkitektur | | Gisle |
| 1.9.6 | Tekninsk infrastrukturarkitektur |  | Gisle |
| 1.9.7 | Sikkerhetsarkitektur | 0.2 | Gisle |
| 1.10 | **Sikkerhet** | - | - |
| 1.10.1 | Tilgangsstyring og brukeradmistrasjon | 0.5 | Gisle |
| 1.10.2 | Sikkerhetsrevisjon | | |
| 1.10.3 | Logging og rapportering | - | Gisle |
